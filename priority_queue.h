#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include "bt.h"
#include "btn.h"
#include "exception.h"
template <class generic>
class Priority_queue : public BT<generic>
{
  public:
    Priority_queue();
    Priority_queue(Priority_queue &);
    Priority_queue & operator= (const Priority_queue &);
    void push( generic x );
    void pop();
    generic & top();
	
	private:
		void heapify(Btn<generic> *);
    Btn<generic> * m_push;
		Btn<generic> * m_pop;

};

#include "priority_queue.hpp"
#endif
