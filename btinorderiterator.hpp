//////////////////////////////////////////////////////////////////////////////
/// @file btinorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the in-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

template <class generic>
BTInorderIterator<generic>::BTInorderIterator ()
{
	m_current = NULL;
}

template <class generic>
BTInorderIterator<generic>::BTInorderIterator (Btn<generic> * x) : m_current (x)
{
	m_current = x;
}

template <class generic>
generic BTInorderIterator<generic>::operator* () const
{
	return *(m_current -> data);
}

template <class generic>
BTInorderIterator<generic> BTInorderIterator<generic>::operator++ ()
{
	//So long as we're not trying to do something stupid
	if(m_current != NULL)
	{
		//Can we go right?
		if(m_current -> r != NULL)
		{
			//Go right-leftmost
			m_current = m_current -> r;
			while(m_current -> l != NULL)
			{
				m_current = m_current -> l;
			}
		}
		else//We need to go up
		{
			//We're not walking off the top of tree and we're the right child
			while(m_current -> p != NULL && m_current -> p -> r == m_current)
			{
				//Go up
				m_current = m_current -> p;
			}
			//Go up once (more)
			m_current = m_current -> p;
		}
	}
	
	return * this;
}

template <class generic>
BTInorderIterator<generic> BTInorderIterator<generic>::operator++ (int)
{
	return ++(*this);
}

template <class generic>
bool BTInorderIterator<generic>::operator== (const BTInorderIterator & x) const
{
	return m_current == x.m_current;
}

template <class generic>
bool BTInorderIterator<generic>::operator!= (const BTInorderIterator & x) const
{
	return m_current != x.m_current;
}

