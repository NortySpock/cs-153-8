//////////////////////////////////////////////////////////////////////////////
/// @file bt.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the bt implementation file
//////////////////////////////////////////////////////////////////////////////

template <class generic>
BT<generic>::BT()
{
	m_size = 0;
	m_root = NULL;
}

template <class generic>
BT<generic>::~BT()
{
	clear();
}

template <class generic>
BT<generic>::BT (BT <generic> & x)
{
	m_size = 0;
	m_root = NULL
	
	*this = x;
}

template <class generic>
BT<generic> & BT<generic>::operator= (const BT &)
{
	
	
	return * this;
}

template <class generic>
void BT<generic>::clear()
{
	//Trying to clear something that's empty
	if(empty())
	{
		//Do nothing
	}
	else//It ain't empty
	{
		//Find the starting node: left-most -> right
		//This will guarantee that we are starting at a leaf node
		Btn<generic> * iter = m_root;
		
		//We're not a leaf node
		while(iter -> l != NULL || iter -> r != NULL)
		{
			//std::cerr << "\n iter at: " << *(iter -> data);
			while(iter -> l != NULL)
			{
				iter = iter -> l;
			}
			
			if(iter -> r != NULL)
			{
				iter = iter -> r;
			}
		}
		
		std::cerr << "\n Iter Starts At: " << *(iter -> data);

		//So now iter is pointing at the correct node
		
		//Now we bring in del, the pointer that will be the delete node.
		Btn<generic> * del = iter;

		//So long as we are not done...
		while(!empty())
		{
			std::cerr << "\n Iter Data: " << *(iter -> data);
			//We're deleting the root node
			if(iter == m_root)
			{
				del = iter;
				delete del -> data;
				delete del;
				m_size--;
				m_root = NULL;//DONE!
			}
			else
			{
				
				//iter is right child, and a leaf node since we're the right child
				if(iter -> p -> r == iter)
				{
					del = iter; //Bring del to where we are, since we're going to delete it.
					iter = iter -> p;//go up
					iter -> r = NULL;//Right pointer to null.
					delete del -> data;
					delete del;
					m_size--;
				}
				else//We're the left child, and a leaf node since we're the left child
				{
					
					del = iter; //Bring del to where we are, since we're going to delete it.
					iter = iter -> p;//go up
					iter -> l = NULL;
					delete del -> data;
					delete del;
					m_size--;
					
					//Parent has a right child
					if (iter -> r != NULL)
					{
						//LET'S GO KILL IT!!! :)
						iter = iter -> r;
						while(iter -> l != NULL)
						{
							iter = iter -> l;
						}//While					
					}//If right child
				}//Else left child
			}//Else not deleting root node
		}//While not empty
	}//Else it ain't empty
}//Clear function

template <class generic>
bool BT<generic>::empty()
{
	if(m_root == NULL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class generic>
unsigned int BT<generic>::size()
{
	return m_size;
}

//Start at root
template <class generic>
BTPreorderIterator<generic> BT<generic>::pre_begin() const
{
	return PreOrder(m_root);
}

//End at right-most node.
template <class generic>
BTPreorderIterator<generic> BT<generic>::pre_end() const
{
	Btn<generic> * temp = NULL;	
	return PreOrder(temp);
}

//Start at left-most node
template <class generic>
BTInorderIterator<generic> BT<generic>::in_begin() const
{
		Btn<generic>* temp = m_root;
		
		//Go left as far as possible
		while(temp -> l != NULL)
		{
			temp = temp -> l;
		}		
		return InOrder(temp);
}

//End at right-most node
template <class generic>
BTInorderIterator<generic> BT<generic>::in_end() const
{
	/*
	Btn<generic>* temp = m_root;
	
	//Go right as far as possible
	while(temp -> r != NULL)
	{
		temp = temp -> r;
	}

	return InOrder(temp);
	*/
	Btn<generic>* temp = NULL;
	return InOrder(temp);
}

template <class generic>
BTPostorderIterator<generic> BT<generic>::post_begin() const
{
	Btn<generic>* temp = m_root;
	
	while(temp -> l != NULL || temp -> r != NULL)
	{
		while(temp -> l != NULL)
		{
			temp = temp -> l;
		}
		
		if(temp -> r != NULL)
		{
			temp = temp -> r;
		}
	}
	
	return PostOrder(temp);
}

template <class generic>
BTPostorderIterator<generic> BT<generic>::post_end() const
{
	Btn<generic>* temp = NULL;
	return PostOrder(temp);
}


template <class generic>
void swap (Btn <generic> * first, Btn <generic> * second)
{
	generic temp;
	
	temp = *(first -> data);
	*(first -> data) = *(second -> data);
	*(second -> data) = temp;
}



