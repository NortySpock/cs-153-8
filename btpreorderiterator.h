//////////////////////////////////////////////////////////////////////////////
/// @file btpreorderiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the pre-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btpreorderiterator
/// @brief The preorder iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////
#ifndef BTPREORDERITERATOR_H
#define BTPREORDERITERATOR_H

#include "btn.h"

template <class generic>
class BTPreorderIterator
{
  public:
    BTPreorderIterator ();
    BTPreorderIterator (Btn<generic> *);
    generic operator* () const;
    BTPreorderIterator operator++ ();
    BTPreorderIterator operator++ (int);
    bool operator== (const BTPreorderIterator &) const;
    bool operator!= (const BTPreorderIterator &) const;
		
    private:
    Btn<generic> * m_current;//The node the iterator is currently pointing at
	
    //The node the iterator was just pointing at.
    //This is to help us see where we have been, so we don't retrace our steps.
    Btn<generic> * m_previous;
	
};

#include "btpreorderiterator.hpp"
#endif

