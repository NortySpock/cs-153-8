//////////////////////////////////////////////////////////////////////////////
/// @file btinorderiterator.h
/// @author Matt Buechler :: CS153 Section 1B
/// @brief This is the in-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btinorderiterator
/// @brief The in order iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////

#ifndef BTINORDERITERATOR_H
#define BTINORDERITERATOR_H

#include "btn.h"

template <class generic>
class BTInorderIterator
{
  public:
    BTInorderIterator ();
    BTInorderIterator (Btn<generic> * x);
    generic operator* () const;
    BTInorderIterator operator++ ();
    BTInorderIterator operator++ (int);
    bool operator== (const BTInorderIterator &) const;
    bool operator!= (const BTInorderIterator &) const;
		
	private:
		Btn<generic> * m_current;//The node the iterator is currently pointing at
};

#include "btinorderiterator.hpp"
#endif

