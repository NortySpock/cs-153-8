//////////////////////////////////////////////////////////////////////////////
/// @file btpostorderiterator.h
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator header file
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
/// @class btpostorderiterator
/// @brief The post-order iterator for a binary tree.
//////////////////////////////////////////////////////////////////////////////
#ifndef BTPOSTORDERITERATOR_H
#define BTPOSTORDERITERATOR_H

#include "btn.h"

template <class generic>
class BTPostorderIterator
{
  public:
    BTPostorderIterator ();
    BTPostorderIterator (Btn<generic> *);
    generic operator* () const;
    BTPostorderIterator operator++ ();
    BTPostorderIterator operator++ (int);
    bool operator== (const BTPostorderIterator &) const;
    bool operator!= (const BTPostorderIterator &) const;
		
	private:
		Btn<generic> * m_current;//The node the iterator is currently pointing at
		
		//The node the iterator was just pointing at.
		//This is to help us see where we have been, so we don't retrace our steps.
		Btn<generic> * m_previous;
};

#include "btpostorderiterator.hpp"
#endif

