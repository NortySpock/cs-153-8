//////////////////////////////////////////////////////////////////////////////
/// @file btpostorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

template <class generic>
BTPostorderIterator<generic>::BTPostorderIterator ()
{
	m_current = NULL;
}

template <class generic>
BTPostorderIterator<generic>::BTPostorderIterator (Btn<generic> * x) : m_current (x)
{
}

template <class generic>
generic BTPostorderIterator<generic>::operator* () const
{
	return *(m_current -> data);
}

template <class generic>
BTPostorderIterator<generic> BTPostorderIterator<generic>::operator++ ()
{
	//We've gone too far, or we're trying to iterate too far
	if(m_current == NULL || m_current -> p == NULL)
	{
		m_current = NULL;
	}
	else//We're guaranteed to have a parent
	{
		//If we're the right child
		if(m_current -> p -> r == m_current)
		{
			//Go up one
			m_current = m_current -> p;
		}		
		else//We're the left child
		{
			//Go up
			m_current = m_current -> p;

			//Parent has right child
			if(m_current -> r != NULL)
			{
				//go right
				m_current =	m_current -> r;
				//left as far as possible
				while(m_current -> l != NULL)
				{
					m_current = m_current -> l;
				}
			}
		}
	}

/*
	if(m_current -> p != NULL && m_current -> p -> l == m_current)//We're the left child
	{
		if(m_current -> p -> r != NULL)//Parent has a right child
		{
			m_current = m_current -> p -> r;
		}
		else//Parent doesn't have a right child
		{
			//Just go up one
			m_current = m_current -> p;
		}
	}
	else
	{
		if(m_current -> p != NULL && m_current -> p -> r == m_current)//We're the right child
		{
			while(m_current -> p != NULL && m_current -> p -> r == m_current)//While we are the right child
			{
				m_current = m_current -> p;
			}
		}
	}
*/

	/*
	bool done = false;
	
	while(!done)
	{
		//Can we go left?
		if(m_current -> l != NULL)
		{
			//go left
			m_current = m_current -> l;
			done = false;
		}
		//Can we go right?
		else if (m_current -> r != NULL)
		{
			//Go right
			m_current = m_current -> r;
			done = false;
		}
		else//We can't go left or right, go up
		{
			if(m_current -> p -> l = m_current)//We're coming up from the left
			{
				if(m_current -> p -> r != NULL)//We can go right from parent
				{
					m_current = m_current -> p -> r;//Go right
					done = false;
				}
				else//we can't go right from parent
				{
					m_current = m_current -> p; //Go up.
					done = true;//Done, since we went up one.
				}
			}
			else//We're coming up from the right
			{
				m_current = m_current -> p;//Go up
				done = true;//Done, since we went up one.
			}			
		}
	}
	
	*/
	return * this;
}

template <class generic>
BTPostorderIterator<generic> BTPostorderIterator<generic>::operator++ (int)
{
	return ++(*this);//Apparently this is an ugly hack for the pre-inc-operator
}

template <class generic>
bool BTPostorderIterator<generic>::operator== (const BTPostorderIterator & x) const
{
	return m_current == x.m_current;
}

template <class generic>
bool BTPostorderIterator<generic>::operator!= (const BTPostorderIterator &x) const
{
	return m_current != x.m_current;
}
