//////////////////////////////////////////////////////////////////////////////
/// @file btpreorderiterator.hpp
/// @author David Norton :: CS153 Section 1B
/// @brief This is the post-order iterator implementation file
//////////////////////////////////////////////////////////////////////////////

template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator ()
{
	m_current = NULL;
}

template <class generic>
BTPreorderIterator<generic>::BTPreorderIterator (Btn<generic> * x) : m_current (x)
{
}

template <class generic>
generic BTPreorderIterator<generic>::operator* () const
{
	return *(m_current -> data);
}

template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ ()
{
	if(m_current !=NULL)
	{
		//std::cerr<<"\n" << *(m_current -> data);
	}
	//We can go left 
	if(m_current -> l != NULL)
	{
		//Go left
		m_current = m_current -> l;
	}
	else if(m_current -> r != NULL)
	{
		//Go right
		m_current = m_current -> r;
	}
	else//Need to go up and over
	{
	//We've gone too far, or we're trying to iterate too far
		if(m_current == NULL)
		{
			//Do nothing
		}
		else
		{
			//go up one
			//m_current = m_current -> p;
			
			while(m_current -> p != NULL && (m_current -> p -> r == NULL || m_current -> p -> r == m_current))
			{
				m_current = m_current -> p;
			}
			if(m_current -> p == NULL)//We hit root, done
			{
				m_current = NULL;
			}
			else//We hit a right child that isn't us.
			{
				//Go up to the right level
				m_current = m_current -> p;
				//go right
				m_current = m_current -> r;
				//done
			}
		}
		
	}
	
		/*
		//so long as we're the left child and we can't go right, we need to go up
		while((m_current -> p -> l == m_current) && (m_current -> r == NULL))
		{
			//Go up
			m_current = m_current -> p;
		}
		//We broke out because we can go right
		if(m_current -> r != NULL)
		{
			//Go right one.
			m_current = m_current -> r;
		}
		else//We broke out because we're no longer the left child of the parent
		{
		}
		*/
	

/*

	//Can we go left?
	if(m_current -> l != NULL)
	{
		//Bring previous up behind us
		m_previous = m_current;
		//Go left
		m_current = m_current -> l;
	}
	//Can we go right?
	else if(m_current -> r != NULL)
	{
		//Bring previous up behind us
		m_previous = m_current;
		
		//Go right
		m_current = m_current -> r;
	}

	else//Gotta go up
	{
		//So long as we don't try to run off the end of the tree, 
		//and we can't go right, go up.
		while(m_current -> p != NULL && m_current -> r == NULL)
		{
			
			//As much as I hear do-whiles are frowned upon, I don't see a better way to do this.
			do
			{
				//Bring previous up behind us
				m_previous = m_current;
				//go up
				m_current = m_current -> p;
				
				
				//So long as we didn't come from the right
			}while(m_current -> l != m_previous);
		}
		//Now that we've gone up as far as the next right node
		if(m_current -> r != NULL)
		{
			//Bring previous up behind us
			m_previous = m_current;
			m_current = m_current -> r;
		}
	}
	
	*/
	return * this;
}

template <class generic>
BTPreorderIterator<generic> BTPreorderIterator<generic>::operator++ (int)
{
	return ++(*this);
}

template <class generic>
bool BTPreorderIterator<generic>::operator== (const BTPreorderIterator & x) const
{
	return m_current == x.m_current;
}

template <class generic>
bool BTPreorderIterator<generic>::operator!= (const BTPreorderIterator & x) const
{
	return m_current != x.m_current;
}

